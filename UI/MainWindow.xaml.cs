﻿using Gucenowicz.CPU.BL;
using Gucenowicz.CPU.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Gucenowicz.CPU.UI
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BusinessLogic businessLogic;
        private ProducersListViewModel producersListViewModel;
        private CPUListViewModel cPUListViewModel;

        private void MenuSwitch(object sender, RoutedEventArgs args)
        {
            MenuItem menu = sender as MenuItem;
            switch (menu.Name)
            {
                case "Producers":
                    DataContext = producersListViewModel;
                    this.Title = "Producenci";
                    break;
                default:
                    DataContext = cPUListViewModel;
                    this.Title = "CPU";
                    break;
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            try
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen;

                Properties.Settings settings = new Properties.Settings();
                businessLogic = new BusinessLogic(settings.dll_path);

                producersListViewModel = new ProducersListViewModel(businessLogic.ProducersBL);
                cPUListViewModel = new CPUListViewModel(businessLogic.CPUsBL);

                DataContext = producersListViewModel;
                this.Title = "Producenci";
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
                Application.Current.Shutdown();
            }
        }
    }
}
