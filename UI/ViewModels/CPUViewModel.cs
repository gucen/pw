﻿using Gucenowicz.CPU.CORE;
using Gucenowicz.CPU.INTERFACES;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.UI.ViewModels
{
    public class CPUViewModel : ViewModel
    {
        private uint _id;
        private string _name;
        private string _socket;
        private string _producer;
        private MemoryList _memoryList;

        public List<MemoryList> MemoryLists => Enum.GetValues(typeof(MemoryList)).Cast<MemoryList>().ToList();

        public CPUViewModel(ICPU cpu)
        {
            _id = cpu.Id;
            _name = cpu.Name;
            _socket = cpu.Socket;
            _producer = cpu.Producer;
            _memoryList = cpu.MemoryList;
        }

        public CPUViewModel()
        {

        }

        public uint Id 
        {
            get
            { 
                return _id; 
            }
            set
            {
                _id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        [Required]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
                Validate();
            }
        }

        [Required]
        public string Socket
        {
            get
            {
                return _socket;
            }
            set
            {
                _socket = value;
                OnPropertyChanged(nameof(Socket));
                Validate();
            }
        }

        [Required]
        public string Producer
        {
            get
            {
                return _producer;
            }
            set
            {
                _producer = value;
                OnPropertyChanged(nameof(Producer));
                Validate();
            }
        }

        [Required]
        public MemoryList MemoryList 
        { 
            get 
            {
                return _memoryList;
            }
            set 
            {
                _memoryList = value;
                OnPropertyChanged(nameof(MemoryList));
                Validate();
            } 
        }
    }
}
