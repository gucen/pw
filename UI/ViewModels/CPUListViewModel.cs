﻿using Gucenowicz.CPU.DAOMockCORE;
using Gucenowicz.CPU.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.UI.ViewModels
{
    public class CPUListViewModel : ListViewModel<ICPU, CPUViewModel>
    {
        public CPUListViewModel(IDAOElem<ICPU> _objBL) : base(_objBL)
        {

        }

        protected override void ListUpdate()
        {
            List.Clear();
            _objBL.GetAll().ForEach(obj => List.Add(new CPUViewModel(obj)));
            OnPropertyChanged(nameof(List));
        }

        protected override void ObjDelete()
        {
            if (Current == null)
            {
                return;
            }

            _objBL.Remove(Current.Id);
            ListUpdate();
        }

        protected override void ObjSave()
        {
            if (Current == null)
            {
                return;
            }

            Current.Validate();
            if (Current.HasErrors)
            {
                return;
            }

            var _dbObj = _objBL.GetById(Current.Id);
            if (_dbObj == null)
            {
                _dbObj = _objBL.Create();
                _objBL.Add(_dbObj);
            }

            _dbObj.Name = Current.Name;
            _dbObj.Producer = Current.Producer;
            _dbObj.Socket = Current.Socket;
            _dbObj.MemoryList = Current.MemoryList;

            ListUpdate();
            Current = new CPUViewModel(_dbObj);
        }

        protected override void ObjCreate()
        {
            Current = new CPUViewModel();
        }
    }
}
