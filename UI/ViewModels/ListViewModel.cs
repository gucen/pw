﻿using Gucenowicz.CPU.INTERFACES;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Gucenowicz.CPU.UI.ViewModels
{
    public class ListViewModel<T, ObjWrapper> : ViewModel
    {
        protected IDAOElem<T> _objBL;
        protected ObjWrapper _current;
        public ObjWrapper Current
        {
            get { return _current; }
            set
            {
                _current = value;
                OnPropertyChanged(nameof(Current));
            }
        }

        public ObservableCollection<ObjWrapper> List { get; set; }

        protected virtual void ListUpdate()
        {

        }

        protected virtual void ObjDelete()
        {

        }

        protected virtual void ObjSave()
        {

        }

        protected virtual void ObjCreate()
        {

        }

        private RelayCommand _deleteCommand;
        private RelayCommand _saveCommand;
        private RelayCommand _createCommand;

        public ListViewModel(IDAOElem<T> objBL)
        {
            _objBL = objBL;
            List = new ObservableCollection<ObjWrapper>();
            _deleteCommand = new RelayCommand(param => this.ObjDelete());
            _saveCommand = new RelayCommand(param => this.ObjSave());
            _createCommand = new RelayCommand(param => this.ObjCreate());

            ListUpdate();
        }

        public ICommand DeleteCommand
        {
            get { return _deleteCommand; }
        }

        public ICommand SaveCommand
        {
            get { return _saveCommand; }
        }

        public ICommand CreateCommand
        {
            get { return _createCommand; }
        }
    }
}
