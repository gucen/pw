﻿using Gucenowicz.CPU.CORE;
using Gucenowicz.CPU.INTERFACES;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.UI.ViewModels
{
    public class ProducerViewModel : ViewModel
    {
        private uint _id;
        private string _name;
        private string _fullName;
        private Country _country;

        public List<Country> Countries => Enum.GetValues(typeof(Country)).Cast<Country>().ToList();

        public ProducerViewModel(IProducer producer)
        {
            _id = producer.Id;
            _name = producer.Name;
            _fullName = producer.FullName;
            _country = producer.Country;
        }

        public ProducerViewModel()
        {

        }

        public uint Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        [Required]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
                Validate();
            }
        }

        [Required]
        public string FullName
        {
            get
            {
                return _fullName;
            }
            set
            {
                _fullName = value;
                OnPropertyChanged(nameof(FullName));
                Validate();
            }
        }

        [Required]
        public Country Country
        {
            get
            {
                return _country;
            }
            set
            {
                _country = value;
                OnPropertyChanged(nameof(Country));
                Validate();
            }
        }
    }
}
