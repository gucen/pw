﻿using Gucenowicz.CPU.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.UI.ViewModels
{
    public class ProducersListViewModel : ListViewModel<IProducer, ProducerViewModel>
    {
        public ProducersListViewModel(IDAOElem<IProducer> _objBL) : base(_objBL)
        {

        }

        protected override void ListUpdate()
        {
            List.Clear();
            _objBL.GetAll().ForEach(obj => List.Add(new ProducerViewModel(obj)));
            OnPropertyChanged(nameof(List));
        }

        protected override void ObjDelete()
        {
            if (Current == null)
            {
                return;
            }

            _objBL.Remove(Current.Id);
            ListUpdate();
        }

        protected override void ObjSave()
        {
            if (Current == null)
            {
                return;
            }

            Current.Validate();
            if (Current.HasErrors)
            {
                return;
            }

            var _dbObj = _objBL.GetById(Current.Id);
            if (_dbObj == null)
            {
                _dbObj = _objBL.Create();
                _objBL.Add(_dbObj);
            }

            _dbObj.Name = Current.Name;
            _dbObj.FullName = Current.FullName;
            _dbObj.Country = Current.Country;

            ListUpdate();
            Current = new ProducerViewModel(_dbObj);
        }

        protected override void ObjCreate()
        {
            Current = new ProducerViewModel();
        }

    }
}
