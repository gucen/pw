﻿using Gucenowicz.CPU.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.INTERFACES
{
    public interface IProducer
    {
        uint Id { get; set; }
        string Name { get; set; }
        string FullName { get; set; }
        Country Country { get; set; }
    }
}
