﻿using Gucenowicz.CPU.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.INTERFACES
{
    public interface ICPU
    {
        uint Id { get; set; }
        string Name { get; set; }
        string Socket { get; set; }
        string Producer { get; set; }
        MemoryList MemoryList { get; set; }
    }
}
