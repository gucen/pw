﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.INTERFACES
{
   public interface IDAOElem<T>
    {
        List<T> GetAll();
        T GetById(uint id);
        T Create();
        bool Add(T obj);
        bool Remove(uint id);
    }
}
