﻿using Gucenowicz.CPU.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.BL
{
    public class ObjectBL<T> : IDAOElem<T>
    {
        private readonly IDAOElem<T> _dao;

        public ObjectBL(IDAOElem<T> dao)
        {
            _dao = dao;
        }

        public bool Add(T obj)
        {
            return _dao.Add(obj);
        }

        public T Create()
        {
            return _dao.Create();
        }

        public List<T> GetAll()
        {
            return _dao.GetAll();
        }

        public T GetById(uint id)
        {
            return _dao.GetById(id);
        }

        public bool Remove(uint id)
        {
            return _dao.Remove(id);
        }
    }
}
