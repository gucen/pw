﻿using Gucenowicz.CPU.INTERFACES;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.BL
{
    public class BusinessLogic
    {
        private string _dllPath;
        private IDAO _dao;
        public IDAOElem<IProducer> ProducersBL { get; set; }
        public IDAOElem<ICPU> CPUsBL { get; set; }

        public BusinessLogic(string dllPath)
        {
            _dllPath = dllPath;

            AssemblyLoad();
        }

        private void ThrowDLLException(string message)
        {
            throw new Exception(message + ": " + _dllPath);
        }

        private void AssemblyLoad()
        {
            try
            {
                var dll = Assembly.UnsafeLoadFrom(@_dllPath);
                List<Type> types = (from type in dll.GetTypes()
                    where typeof(IDAO).IsAssignableFrom(type)
                    select type).ToList();

                if (types.Count != 1)
                {
                    throw new Exception("Niepoprawny format bazy");
                }

                _dao = (IDAO)Activator.CreateInstance(types[0]);
                ProducersBL = new ProducersBL(_dao.DAOProducers);
                CPUsBL = new CPUsBL(_dao.DAOCPUs);
            }
            catch (Exception e)
            {
                ThrowDLLException(e.Message);
                return;
            }
        }
    }
}
