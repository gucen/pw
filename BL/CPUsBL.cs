﻿using Gucenowicz.CPU.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.BL
{
    public class CPUsBL : ObjectBL<ICPU>
    {
        public CPUsBL(IDAOElem<ICPU> dao) : base(dao)
        {

        }
    }
}
