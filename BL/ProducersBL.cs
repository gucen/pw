﻿using Gucenowicz.CPU.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.BL
{
    public class ProducersBL : ObjectBL<IProducer>
    {
        public ProducersBL(IDAOElem<IProducer> dao) : base(dao)
        {

        }
    }
}
