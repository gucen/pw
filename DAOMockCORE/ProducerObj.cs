﻿using Gucenowicz.CPU.CORE;
using Gucenowicz.CPU.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.DAOMockCORE
{
    public class ProducerObj : IProducer
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public Country Country { get; set; }
    }
}
