﻿using Gucenowicz.CPU.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.DAOMockCORE
{
    public class DAOProducersCore : IDAOProducers
    {
        protected List<IProducer> _producers;

        public DAOProducersCore()
        {
            _producers = new List<IProducer>();
        }

        public bool Add(IProducer obj)
        {
            if (obj.Id < 1)
            {
                obj.Id = _producers.Count > 0 ? (_producers.Last().Id + 1) : 1;
            }

            _producers.Add(obj);

            return true;
        }

        public IProducer Create()
        {
            return new ProducerObj();
        }

        public List<IProducer> GetAll()
        {
            return _producers;
        }

        public IProducer GetById(uint id)
        {
            return _producers.FirstOrDefault(obj => obj.Id == id);
        }

        public bool Remove(uint id)
        {
            return _producers.RemoveAll(obj => obj.Id == id) > 0;
        }
    }
}
