﻿using Gucenowicz.CPU.CORE;
using Gucenowicz.CPU.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.DAOMockCORE
{
    public class CPUObj : ICPU
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string Socket { get; set; }
        public string Producer { get; set; }
        public MemoryList MemoryList { get; set; }
    }
}
