﻿using Gucenowicz.CPU.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.DAOMockCORE
{
    public class DAOCPUsCore : IDAOCPUs
    {
        protected List<ICPU> _CPUs;

        public DAOCPUsCore()
        {
            _CPUs = new List<ICPU>();
        }

        public bool Add(ICPU obj)
        {
            if (obj.Id < 1)
            {
                obj.Id = _CPUs.Count > 0 ? (_CPUs.Last().Id + 1) : 1;
            }

            _CPUs.Add(obj);

            return true;
        }

        public ICPU Create()
        {
            return new CPUObj();
        }

        public List<ICPU> GetAll()
        {
            return _CPUs;
        }

        public ICPU GetById(uint id)
        {
            return _CPUs.FirstOrDefault(obj => obj.Id == id);
        }

        public bool Remove(uint id)
        {
            return _CPUs.RemoveAll(obj => obj.Id == id) > 0;
        }
    }
}
