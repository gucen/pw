﻿using Gucenowicz.CPU.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.DAOMock1
{
    class DAOMock : IDAO
    {
        private IDAOProducers _daoProducers = new DAOProducers();
        private IDAOCPUs _daoCPUs = new DAOCPUs();

        public IDAOProducers DAOProducers
        {
            get
            {
                return _daoProducers;
            }
        }

        public IDAOCPUs DAOCPUs
        {
            get
            {
                return _daoCPUs;
            }
        }
    }
}
