﻿using Gucenowicz.CPU.CORE;
using Gucenowicz.CPU.DAOMockCORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.DAOMock1
{
    class DAOCPUs : DAOCPUsCore
    {
        public DAOCPUs() : base()
        {
            Add(new CPUObj
            {
                Producer = "Intel",
                MemoryList = MemoryList.DDR4,
                Name = "Core i3-9100F",
                Socket = "1151"
            });

            Add(new CPUObj 
            {
                Producer = "Intel",
                MemoryList = MemoryList.DDR4,
                Name = "Core i5-9600K",
                Socket = "1151"
            });

            Add(new CPUObj
            {
                Producer = "Intel",
                MemoryList = MemoryList.DDR4,
                Name = "Core i9-9900K",
                Socket = "1151"
            });

            Add(new CPUObj
            {
                Producer = "Intel",
                MemoryList = MemoryList.DDR3,
                Name = "Core i5-3570K",
                Socket = "1155"
            });

            Add(new CPUObj
            {
                Producer = "AMD",
                MemoryList = MemoryList.DDR4,
                Name = "Ryzen 5 1600 AF",
                Socket = "AM4"
            });

            Add(new CPUObj
            {
                Producer = "AMD",
                MemoryList = MemoryList.DDR4,
                Name = "Ryzen 5 2600",
                Socket = "AM4"
            });
        }
    }
}
