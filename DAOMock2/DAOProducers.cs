﻿using Gucenowicz.CPU.CORE;
using Gucenowicz.CPU.DAOMockCORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.DAOMock2
{
    class DAOProducers : DAOProducersCore
    {
        public DAOProducers() : base()
        {
            Add(new ProducerObj
            {
                Country = Country.USA,
                Name = "Intel",
                FullName = "Intel"
            });

            Add(new ProducerObj
            {
                Country = Country.USA,
                Name = "AMD",
                FullName = "Advanced Micro Devices"
            });
        }
    }
}
