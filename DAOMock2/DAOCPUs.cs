﻿using Gucenowicz.CPU.CORE;
using Gucenowicz.CPU.DAOMockCORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gucenowicz.CPU.DAOMock2
{
    class DAOCPUs : DAOCPUsCore
    {
        public DAOCPUs() : base()
        {
            Add(new CPUObj
            {
                Producer = "Intel",
                MemoryList = MemoryList.DDR2,
                Name = "Core 2 Duo E8400",
                Socket = "775"
            });

            Add(new CPUObj
            {
                Producer = "Intel",
                MemoryList = MemoryList.DDR2,
                Name = "Core 2 Duo E7300",
                Socket = "775"
            });

            Add(new CPUObj 
            {
                Producer = "Intel",
                MemoryList = MemoryList.DDR2,
                Name = "Core 2 Quad Q9400",
                Socket = "775"
            });

            Add(new CPUObj
            {
                Producer = "Intel",
                MemoryList = MemoryList.DDR,
                Name = "Pentium 4",
                Socket = "478"
            });

            Add(new CPUObj
            {
                Producer = "Intel",
                MemoryList = MemoryList.DDR,
                Name = "Celeron",
                Socket = "478"
            });

            Add(new CPUObj
            {
                Producer = "AMD",
                MemoryList = MemoryList.DDR2,
                Name = "Athlon II X2 250",
                Socket = "AM2"
            });

            Add(new CPUObj
            {
                Producer = "AMD",
                MemoryList = MemoryList.DDR3,
                Name = "Athlon II X4 640",
                Socket = "AM3"
            });
        }
    }
}
